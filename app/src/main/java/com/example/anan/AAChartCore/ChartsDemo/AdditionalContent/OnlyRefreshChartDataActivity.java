package com.example.anan.AAChartCore.ChartsDemo.AdditionalContent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.anan.AAChartCore.R;

public class OnlyRefreshChartDataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_only_refresh_chart_data);
    }
}
