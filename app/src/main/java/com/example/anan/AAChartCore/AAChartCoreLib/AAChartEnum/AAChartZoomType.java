package com.example.anan.AAChartCore.AAChartCoreLib.AAChartEnum;

public interface AAChartZoomType {
    String X  = "x";
    String Y  = "y";
    String XY = "xy";
}
